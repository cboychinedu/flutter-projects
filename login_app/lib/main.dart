//
import "package:flutter/material.dart";
import './ui/login.dart';

// Running the main function
void main() {
  runApp(new MaterialApp(
    title: "Login",
    home: new Login()
  ));
}