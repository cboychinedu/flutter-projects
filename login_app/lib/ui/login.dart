//
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginState ();
  }

}

class LoginState extends State<Login> {
  //
  final TextEditingController _userController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();

  //
  var _welcomeString =  "you can login Here!";

  //
  void _erase() {
    setState(() {
      _userController.clear();
      _passwordController.clear();
      _welcomeString = "you can login Here!";
    });
  }

  // Get the username
  void _getUsername() {
    var username = _userController.text.toString();

    //
    setState(() {
      _welcomeString = username;
    });
  }

  //
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
        centerTitle: true,
        backgroundColor: Colors.green,
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: [
            // Image profile
            Image.asset(
              "Images/login.png",
              width: 200.0,
              height: 200.0,
            ),
            // Adding the forms
            Container (
              height: 380.0,
              width: 400.0,
              // color: Colors.black12,

              padding: EdgeInsets.only(top: 30.0, left: 30.0, right: 30.0),
              child: Column(
                children: [
                  Container(
                    child: TextField(
                      controller: _userController,
                      decoration: InputDecoration(
                          hintText: "Username",
                          icon: Icon(Icons.person)
                      ),
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10.0),
                    child: TextField(
                      controller: _passwordController,
                      decoration: InputDecoration(
                          hintText: "Password",
                          icon: Icon(Icons.lock)
                      ),
                      obscureText: true,
                    ),
                  ),

                  Container(
                      margin: EdgeInsets.only(top: 30.0, left: 35.0),
                    width: 300.0,
                    child: RaisedButton(
                      elevation: 20,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                      onPressed: _getUsername,
                      color: Colors.blue,
                      padding: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 20.0, right: 20.0),
                      child: Text("Submit", style: TextStyle(color: Colors.white70)),)
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 15.0, left: 35.0),
                      width: 300.0,
                      child: RaisedButton(
                        elevation: 20,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                        onPressed: _erase,
                        color: Colors.red,
                        padding: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 20.0, right: 20.0),
                        child: Text("Clear", style: TextStyle(color: Colors.white70)),)
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 9.0),
                    padding: EdgeInsets.only(top: 14.0),
                    child: Text("Welcome ${_welcomeString}",)
                  )

                ],
              )
            )
          ],
        )
      ),
    );
  }

}