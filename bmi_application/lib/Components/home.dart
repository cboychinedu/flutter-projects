/*
This application is for calculation of
the body mass index.
Metric Units: BMI = Weight (kg) / (Height (m) x Height (m))
*********

Weight	BMI
Underweight	Below 18.5
Normal	18.5–24.9
Overweight	25.0–29.9
Obesity	30.0 and Above
 */


// Importing the necessary modules
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import "./bmi_calculator.dart";

// Creating a class for the stateful widget
class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the stateful widget
    return HomeStateful();
  }
}

// Creating the class home stateful
class HomeStateful extends State<Home> {
  // Creating the object controllers for the text fields
  final TextEditingController _inputAgeController = new TextEditingController();
  final TextEditingController _inputHeightController = new TextEditingController();
  final TextEditingController _inputWeightController = new TextEditingController();

  // Result
  String _result = "Your BMI is: 0.00";
  String WeightValue = "";
  double res = 0.00;
  double _weight = 0.00;
  double _height = 0.00;

  // Creating a function for handling the submit
  void HandleSubmit() {
    // Checking if the form field is not empty
    if (_inputAgeController.text.isNotEmpty && _inputWeightController.text.isNotEmpty
    && _inputWeightController.text.isNotEmpty ) {
      _weight  = double.parse(_inputWeightController.text);
      _height = double.parse(_inputHeightController.text);
      _height = _height * 12;
      res = CalculateBmi(_weight, _height);

      // Checking for the category for Normal, Overweight, and
      // Obesity
      // Normal
      if (res < 24.9 ) {
        // Normal
        WeightValue = "Normal";
      }
      // Overweight
      else if (res == 25.0 || res < 29.9) {
        // Overweight
        WeightValue = "Overweight";
      }
      // Obesity
      else if (res == 29.9 || res >= 30.0 ) {
        // Obesity
        WeightValue = "Obesity";
      }

      // Setting the state
      setState(() {
        _result = "Your BMI is: ${res.toStringAsFixed(3)}";
      });

    }

    // Else
    else {
      // Execute the block of code if one of the values are missing
      // Set the state
      setState(() {
        _result = "Age, height or weight is missing.";
      });
    }

  }

  // Creating an override method for drawing the widgets
  @override
  Widget build(BuildContext context) {
    // Return the scaffold widget
    return Scaffold(
      // Adding the application appbar
      appBar: AppBar(
        title: const Text("Bmi Calculator"),
        backgroundColor: Colors.deepOrange.shade600,
        actions: <Widget> [
          IconButton(onPressed: () => print("Send"), icon: Icon(Icons.search))
        ],

      ),

      // Adding the body
      body: Container(
        margin: const EdgeInsets.only(top: 20.0),
        child: ListView(
          padding: const EdgeInsets.all(10.0),
          children: <Widget> [
              // Adding the image
              Image.asset("images/bmi.jpg", height: 200.0, width: 200.0 ),

              // Adding the second container to hold the age text field
              Container(
                margin: const EdgeInsets.only(top: 20.0),
                padding: const EdgeInsets.only(right: 70.0, left: 30.0 ),
                width: 100.0,
                alignment: Alignment.center,
                child:  TextField(
                    controller: _inputAgeController,
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                        labelText: "Your age",
                        hintText: "Your age in years",
                        icon: Icon(Icons.person)
                    ),
                )
              ),

              // Adding the third container to hold the height text field
              Container(
                  margin: const EdgeInsets.only(top: 20.0),
                  padding: const EdgeInsets.only(right: 70.0, left: 30.0 ),
                  width: 100.0,
                  alignment: Alignment.center,
                  child: TextField(
                      controller: _inputHeightController,
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(
                          labelText: "Your height",
                          hintText: "Your height in feet",
                          icon: Icon(Icons.monitor_weight)
                      ),
                  )
              ),

              // Adding the fourth container to hold the weight in pounds
              Container(
                  margin: const EdgeInsets.only(top: 20.0),
                  padding: const EdgeInsets.only(right: 70.0, left: 30.0 ),
                  width: 50.0,
                  alignment: Alignment.center,
                  child: TextField(
                      controller: _inputWeightController,
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(
                          labelText: "Your weight",
                          hintText: "Your weight in pounds",
                          icon: Icon(Icons.monitor_weight)
                      ),
                  ),
              ),

              // Adding the button container
              Container(
                  margin: const EdgeInsets.only(top: 30.0),
                  padding: const EdgeInsets.only(right: 70.0, left: 70.0 ),
                  width: 300.0,
                  height: 40.0,
                  child: RaisedButton(
                      onPressed: HandleSubmit,
                      elevation: 10,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6.0)),
                      color: Colors.blue.shade500,
                      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
                      child: const Text("Get Bmi Value", style: TextStyle(color: Colors.white)),
                  )
              ),

              // Adding the result container
              Container(
                  padding: const EdgeInsets.only(top: 30.0, left: 70.0, right: 70.0),
                  child: Text(
                      "${_result}",
                      style: const TextStyle(
                        color: Colors.blue,
                        fontSize: 20.6,
                        fontWeight: FontWeight.w500
                      )
                  )
              ),

              // Adding the second result container
              Container(
                  padding: const EdgeInsets.only(top: 10.0, left: 70.0, right: 70.0),
                  child: Text(
                      "${WeightValue}",
                      style: const TextStyle(
                        color: Colors.red,
                        fontSize: 18.0,
                        fontWeight: FontWeight.w700
                      )
                  )
              ),


              // Adding the result container



          ],
        )
      ),
    );
  }
  //
}