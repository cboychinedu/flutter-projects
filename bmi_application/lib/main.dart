/*
This application is for calculation of
the body mass index.
Metric Units: BMI = Weight (kg) / (Height (m) x Height (m))
*********

Weight	BMI
Underweight	Below 18.5
Normal	18.5–24.9
Overweight	25.0–29.9
Obesity	30.0 and Above
 */

// Importing the necessary modules
import 'package:flutter/material.dart';
import 'Components/home.dart';

// Running the main function
void main() {
  runApp(
    MaterialApp(
      title: "Bmi Calculator",
      home: Home(),
    )
  );
}