// Importing the necessary modules
import 'package:flutter/material.dart';
import 'Components/home.dart';

// Running the main function
void main() {
  runApp(
    MaterialApp(
      title: "Weight on planet X",
      home: Home(),
    )
  );
}

