// Importing the necessary modules
import 'package:flutter/material.dart';

// Creating a class for the stateful widget
class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the stateful widget
    return HomeStateful();
  }
}

// Creating the class home stateful
class HomeStateful extends State<Home> {
  // Creating the text editing controller
  final TextEditingController _weightController = new TextEditingController();

  // Creating variables
  int radioValue = 0;
  double _finalResult = 0.00;
  double weight = 0.00;
  String _formatedText = "";


  //
  void handleRadioValueChanged(value) {
    //
    setState(() {
        //
        radioValue = value;

        // Using switch case for the conversion
        if (_weightController.text.isNotEmpty) {
          weight = double.parse(_weightController.text);
        }
        else {
          weight = 0.00;
        }
        // Using switch case statement
        switch (radioValue) {
        case 0:
          _finalResult = calculateWeight(weight, 0.06);
          _formatedText = "your weight on Pluto is: ${_finalResult.toStringAsFixed(2)}";
          break;
        case 1:
          _finalResult = calculateWeight(weight, 0.38);
          _formatedText = "your weight on Mars is: ${_finalResult.toStringAsFixed(2)}";
          break;
        case 2:
          _finalResult = calculateWeight(weight, 0.91);
          _formatedText = "your weight on Venus is: ${_finalResult.toStringAsFixed(2)}";
          break;

        }

    });
  }
  @override
  Widget build(BuildContext context) {
    // Returning a scaffold
    return Scaffold(
      backgroundColor: Colors.white70,
      // Adding the application appbar
      appBar: AppBar(
        title: const Text("Weight on planet X"),
        backgroundColor: Colors.amber.shade600,
      ),
      // Adding the body
      body: Container(
        margin: const EdgeInsets.only(top: 10.0),
        child: ListView(
          padding: const EdgeInsets.all(2.5),
          children: [
            Image.asset(
                "images/img.jpg",
                height: 133.0,
                width: 200.0,
            ),

            // Adding the second container
            Container(
              margin: const EdgeInsets.only(top: 20.0),
              width: 100.0,
              padding: EdgeInsets.only(right: 40.0),
              alignment: Alignment.center,
              child: Column(
                children: <Widget> [
                    // Adding the text field container
                    Container(
                      padding: EdgeInsets.only(left: 20.0),
                      child: TextField(
                        controller: _weightController,
                        keyboardType: TextInputType.number,
                        decoration: const InputDecoration(
                            labelText: "Your Weight on Earth",
                            hintText: "In Pounds",
                            icon: Icon(Icons.person_outline)
                        ),
                      ),
                    ),

                  // Adding the radio buttons container inside a row
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget> [
                      // Adding the radio buttons
                      Radio (
                        activeColor: Colors.brown,
                         groupValue: radioValue, onChanged: handleRadioValueChanged, value: 0,
                      ),
                      Text("Pluto", style: TextStyle(color: Colors.black),),

                      Radio (
                        activeColor: Colors.red,
                        groupValue: radioValue, onChanged: handleRadioValueChanged, value: 1,
                      ),
                      Text("Mars", style: TextStyle(color: Colors.black),),

                      Radio (
                        activeColor: Colors.orangeAccent,
                        groupValue: radioValue, onChanged: handleRadioValueChanged, value: 2,
                      ),
                      Text("Venus", style: TextStyle(color: Colors.black),)
                    ],
                  ),

                  // Adding the result container
                  Container(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Text(
                      "${_formatedText}lbs",
                      style: const TextStyle(
                        color: Colors.red,
                        fontSize: 19.4,
                        fontWeight: FontWeight.w500,
                      )
                    )
                  ),
                ],
              )
            )
          ],
        )
      ),
    );
  }

  double calculateWeight(double weight, double d) {
    return weight * d;
  }


}