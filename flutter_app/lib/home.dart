//
import "package:flutter/material.dart";

// Creating a class home
class Home extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // Setting up the scaffold
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.greenAccent.shade700,
        title: const Text("Fency Day"),
        actions: <Widget>[
          IconButton(
              onPressed: () => debugPrint("Icon Tapped."),
              icon: const Icon(Icons.send)),
          IconButton(
              onPressed: () => debugPrint("Icon Tapped."),
              icon: const Icon(Icons.search)),


        ],
      ),

      // Setting the background color of the scaffold
      backgroundColor: Colors.white70,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
              const Text("Bonni",
                style: TextStyle(
                  fontSize: 14.5,
                  fontWeight: FontWeight.w400,
                  color: Colors.black
                )),

              InkWell(
                child: const Text("Send"),
                onTap: () => debugPrint("Button Tapped"),
            ),
          ],
        )
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: null,
        backgroundColor: Colors.lightGreen,
        tooltip: "Going Up!",
        child: Icon(Icons.send),
      ),
      bottomNavigationBar: BottomNavigationBar(items: const [
        BottomNavigationBarItem(
          label: "Home",
          icon: Icon(Icons.home),
          backgroundColor: Colors.black

        ),
        BottomNavigationBarItem(
          label: "Search",
          icon: Icon(Icons.search),
        ),
        BottomNavigationBarItem(
          label: "Categories",
          icon: Icon(Icons.grid_view),
        ),
        BottomNavigationBarItem(
          label: "My Account",
          icon: Icon(Icons.account_circle_outlined),
        ),
      ], onTap: (int i) => debugPrint("Hey Touched ${i}"),),
    );
  }

}