// Importing the necessary modules
import 'package:flutter/material.dart';
import "home.dart";

// Running the main function
void main() {
  runApp(
    new MaterialApp(
      title: "Scaffold",
      home: new Home(),
    )
  );
}