// Importing the necessary modules
import 'package:flutter/material.dart';

// Extending the stateless widget
class MakeItRain extends StatefulWidget {
  const MakeItRain({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MakeItRainState();
  }

}

//
class MakeItRainState extends State<MakeItRain> {
  // Creating the money counter
  double _moneyCounter = 0;

  void _rainMoney() {
    // Incrementing the state money counter.
    // N/B set state is called whenever we want to increment the money counter
    setState(() {
      // Checking
      if (_moneyCounter == 10000) {
        //

      }
      _moneyCounter = _moneyCounter + 100;
    });

    // print(_moneyCounter);
  }

  // Reset the counter
  void _resetCounter() {
    // When this function is clicked, the counter is set to zero
    setState(() {
      _moneyCounter = 0.00;
    });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: const Text("Make It Rain"),
        backgroundColor: Colors.lightGreen,
      ),
      body: Container(
        child: Column(
          children: <Widget> [
            Center(
              child: Container (
                  margin: EdgeInsets.only(top: 20.0, bottom: 20.56),
                  child: Text("Get Rich",
                      style: TextStyle(
                          color: Colors.deepOrange,
                          fontWeight: FontWeight.w700,
                          fontSize: 40.8))
              )
            ),
            Center(
              child: Text("Press the button to increase your funds")
            ),
            Expanded(
                child: Center(
                  child: Text(
                      '\$${_moneyCounter}',
                      style: TextStyle(
                        color: _moneyCounter > 2000 ? Colors.greenAccent : Colors.amberAccent,
                        fontSize: _moneyCounter > 1000 ? 56.9 : 12.98,
                    fontWeight: FontWeight.w800,
                  ))
                )
            ),
            Expanded(
                child: Center(
                  child: RaisedButton(
                      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 20.0),
                      onPressed: _rainMoney,
                      child: Text("Let It Rain!",
                      style: TextStyle(
                        fontSize: 19.9,

                      ),)

                  )
                )),
            Center (
              child: Container (
                margin: EdgeInsets.only(bottom: 200.0),
                child: Column (
                children: <Widget> [
                  Text("Press the button below to reset the counter"),
                  RaisedButton(
                      onPressed: _resetCounter,
                      padding: EdgeInsets.only(top: 20.0, bottom: 20.0, left: 20.0, right: 20.0),
                      child: Text("Reset Counter",),
                       color: Colors.deepOrange,)
                ],
              ) )
            )
            // Container(
            //   margin: const EdgeInsets.only(left: 20.0, right: 80.0, bottom: 200.09),
            //   child: Column (
            //     children:  <Widget> [
            //       Text("Press the button below to reset the counter"),
            //       RaisedButton(
            //         onPressed: _resetCounter,
            //         child: Container(
            //           margin: EdgeInsets.only(left: 30.0),
            //           child: Text("Reset Counter"),
            //         ),
            //         )
            //     ],
            //   )
            // )

          ],
        )
      ),
    );
  }

}