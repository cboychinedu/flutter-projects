// Importing the necessary modules
import 'package:flutter/material.dart';

// Creating a class for the stateful widget
class Home extends StatefulWidget {
  // Return the state for the home application
  @override
  State<StatefulWidget> createState() {
    //
    return StatelessHome();
  }
}

// Creating the stateless home
class StatelessHome extends State<Home> {
  //
  final TextEditingController _inputController = new TextEditingController();
  final TextEditingController _inputFahreheitController = new TextEditingController();
  final TextEditingController _inputCelsisusController = new TextEditingController();

  // Creating the result variable
  var _result = "0.00";

  // Creating a function for converting the value into Fahrenheit
  void ConvertToFahrenheit() {
    print("Convert to fahrenheit");
  }

  void ConvertFahrenheitToCelsius() {
    print("");
  }
  // Creating a function for converting the values into kelvin
  void ConvertToKelvin() {
    var res = _inputController.text.toString();
    double res_double = double.parse(res);
    res_double = res_double + 273.0;

    // Changing the state for the result
    setState(() {
      _result = "${res_double}K";
    });
  }

  // Creating a function for converting the values into celsius
  void ConvertToCelsius() {
    var res = _inputController.text.toString();
    double res_double = double.parse(res);
    res_double = res_double - 273.0;

    // Changing the state for the result
    setState(() {
      _result = "${res_double}C";
    });
  }

  @override
  Widget build(BuildContext context) {
    // Return the scaffold
    return Scaffold(
      // Adding the application appbar
      appBar: AppBar(
          title: Text("Temperature Converter"),
          centerTitle: true,
          backgroundColor: Colors.green.shade600,
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          children: <Widget> [
            Container(
              margin: EdgeInsets.only(top: 30.0),
              width: 300.0,
              height: 150.0,
              child: Center (
                child: Text(
                  "${_result}",
                  style: TextStyle(
                      fontSize: 30.0,
                      color: Colors.amber.shade700
                  ),
                ),
              ),
              color: Colors.black12,
            ),
            Container(
              width: 300.0,
              padding: EdgeInsets.only(right: 10.0),
              margin: EdgeInsets.only(top: 10.0),
              child: TextField(
                controller: _inputController,
                decoration: InputDecoration(
                  hintText: "Type the temperature value here",
                  icon: Icon(Icons.heat_pump_rounded)
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 30.0, left: 1.0),
              width: 300.0,
              height: 40.0,
              child: RaisedButton(
                onPressed: ConvertToCelsius,
                elevation: 10,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                color: Colors.red,
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0 ),
                child: Text("Convert to celsius", style: TextStyle(color: Colors.white70)),

              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 30.0, left: 1.0),
              width: 300.0,
              height: 40.0,
              child: RaisedButton(
                onPressed: ConvertToKelvin,
                elevation: 10,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                color: Colors.black45,
                padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0 ),
                child: Text("Convert to Kelvin", style: TextStyle(color: Colors.white70)),

              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0, left: 1.0),
              width: 300.0,
              child: Column (
                children: <Widget> [
                  Container(
                    padding: EdgeInsets.only(right: 20.0),
                    margin: EdgeInsets.only(top: 6.0),
                    child: TextField(
                        controller: _inputCelsisusController,
                        decoration: InputDecoration(
                            hintText: "Convert from Celsius to Fahreheit",
                            icon: Icon(Icons.heat_pump_rounded)
                        ),
                    )
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 20.0),
                    margin: EdgeInsets.only(top: 10.0),
                    child: TextField(
                        controller: _inputFahreheitController,
                        decoration: InputDecoration(
                          hintText: "Convert from Fahrenheit to Celsius",
                          icon: Icon(Icons.heat_pump)
                        ),
                    )
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30.0, left: 1.0),
                    width: 300.0,
                    height: 40.0,
                    child: RaisedButton(
                      onPressed: ConvertToFahrenheit,
                      elevation: 10,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                      color: Colors.deepOrange.shade700,
                      padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0 ),
                      child: Text("Convert Celsius to F", style: TextStyle(color: Colors.white70)),

                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 13.0, left: 1.0),
                    width: 300.0,
                    height: 40.0,
                    child: RaisedButton(
                      onPressed: ConvertFahrenheitToCelsius,
                      elevation: 10,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                      color: Colors.deepOrange.shade700,
                      padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0 ),
                      child: Text("Convert Fahrenheit to C", style: TextStyle(color: Colors.white70)),

                    )
                  )
                ],
              )
            )

          ],
        )
      )
    );
  }

}