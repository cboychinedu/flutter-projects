// Importing the necessary modules
import "package:flutter/material.dart";
import 'Components/weather_app.dart';

// Running the main application
void main() {
  runApp(
    MaterialApp(
      title: "Weather Application",
      home: new WeatherApp(),
    )
  );
}