// Importing the necessary modules
import "dart:async";
import "dart:convert";
import "package:http/http.dart" as http;
import 'package:flutter/material.dart';
import '../Components/utils.dart';

// Running the weather application
class WeatherApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the state
    return WeatherAppState();
  }
}

//
class WeatherAppState extends State<WeatherApp> {
  // Setting the name of the search query
  String _location = "Lagos State";
  String _country = "Nigeria";
  String _region = "Lagos";
  String _longitude = "34.56";
  String _latitude = "-98.09";
  String _time = "8:30 PM";
  String _localtime = "8:30 PM";
  String _temperature = "27";
  String _wind_speed = "6km/hr";
  String _wind_direction = "SW";
  String _pressure = "1014millibar";
  String _humidity = "79";

  // Creating the object controllers for the search State
  final TextEditingController _searchLocation = TextEditingController();

  // Show the
  void getTheWeather() async {
    String apiKey = "fdd517c68aa7e08787c58fe155d2dc92";
    String city = "";

    // Checking if the form field is not empty
    if (_searchLocation.text.isNotEmpty) {
      // Execute the block of code if the search location is not empty
      city = _searchLocation.text;

      // Connecting to the api severs
      Map data = await getWeather(apiKey, city);
      print(data);

      // Working on the data
      setState(() {
        // Setting the search result
        _location = data["location"]["name"].toString();
        _region = data["location"]["region"].toString();
        _country = data["location"]["country"].toString();
        _longitude = data["location"]["lon"].toString();
        _latitude = data["location"]["lat"].toString();
        _localtime = data["location"]["localtime"].toString();
        _temperature = data["current"]["temperature"].toString();
        _wind_speed = data["current"]["wind_speed"].toString();
        _wind_direction = data["current"]["wind_dir"].toString();
        _time = data["current"]["observation_time"].toString();
      });
      // print(data);
    }

    // Map data = await getWeather(apiKey, city);
    // print(data.toString());
  }

  @override
  Widget build(BuildContext context) {
    //
    return Scaffold(
      appBar: AppBar(
        elevation: 10,
        backgroundColor: Color(0xFF421e09),
        actions: <Widget>[
          IconButton(
              onPressed: () => debugPrint("hello"), icon: Icon(Icons.menu))
        ],
        title: const Text("Weather App"),
      ),
      body: Stack(
        children: [
          Center(
            child: Image.asset(
              "Images/weather.jpg",
              width: 490,
              height: 1200.0,
              fit: BoxFit.fill,
            ),
          ),
          // Adding the first container
          Container(
            // alignment: Alignment.center,
            alignment: Alignment.topLeft,
            margin: const EdgeInsets.only(top: 20.0, left: 25.0, right: 10.0),
            child: Text(
              "${_location}",
              style: cityStyle(),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 37.0, left: 25.0),
            child: Row(
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(
                    top: 6.0,
                  ),
                  child: Text(
                    "${_region}",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(top: 6.0, left: 10.0),
                  child: Text("Pressure: $_pressure",
                      style: TextStyle(color: Colors.white)),
                ),
              ],
            ),
          ),
          Container(
              margin: const EdgeInsets.only(top: 50.0, left: 15.0),
              child: Row(
                children: [
                  Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(top: 10.0, left: 10.0),
                    child: Text("Humidity: $_humidity%",
                        style: TextStyle(color: Colors.white)),
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(top: 10.0, left: 6.0),
                    child: Text("$_wind_speed km/s",
                        style: TextStyle(color: Colors.white)),
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(top: 10.0, left: 6.0),
                    child: Text("Direction: $_wind_direction",
                        style: TextStyle(color: Colors.white)),
                  ),
                ],
              )),
          Container(
            margin: EdgeInsets.only(top: 77.0, left: 24.0),
            child: Row(
              children: [
                Text("${_time}", style: TextStyle(color: Colors.white, fontSize: 25.0),)
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 12.0, top: 150.0),
            child: Row(
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(top: 6.0, left: 10.0),
                  child: Text("Lat: $_latitude",
                      style: TextStyle(color: Colors.white)),
                ),
                Container(
                  alignment: Alignment.topLeft,
                  margin: const EdgeInsets.only(top: 6.0, left: 10.0),
                  child: Text("Long: $_longitude",
                      style: TextStyle(color: Colors.white)),
                ),
              ],
            ),
          ),
          Container(
              alignment: Alignment.topRight,
              margin: const EdgeInsets.only(top: 20.0, right: 20.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: Image.asset(
                  "Images/weather.png",
                  height: 50.0,
                  width: 50.0,
                ),
              )),
          Container(
            alignment: Alignment.topLeft,
            margin: const EdgeInsets.only(top: 110.0, left: 20.0),
            child: Text(
              "${_temperature} °C",
              style: tempStyle(),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 240.0, left: 20.0),
            alignment: Alignment.topCenter,
            width: 200.0,
            height: 50.0,
            child: TextField(
              style: const TextStyle(color: Colors.white),
              controller: _searchLocation,
              decoration: InputDecoration(
                labelText: "Type your city here",
                labelStyle:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                fillColor: Color(0xFF241106),
                filled: true,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 300.0, left: 20.0),
            width: 100.0,
            height: 40.0,
            child: RaisedButton(
              onPressed: getTheWeather,
              color: Color(0xFF0963b3),
              child: const Text(
                "Get Weather",
                style: TextStyle(color: Colors.white),
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: const Color(0xff5c301a),
          items: const [
            BottomNavigationBarItem(
                icon: Icon(Icons.ac_unit, size: 30, color: Colors.blue),
                label: "Cool Off"),
            BottomNavigationBarItem(
                icon: Icon(Icons.contact_mail,
                    size: 30, color: Color(0xff360a1e)),
                label: "Send Mail"),
            BottomNavigationBarItem(
                icon: Icon(Icons.save, size: 30, color: Color(0xffe85e0e)),
                label: "Save Data")
          ]),
    );
  }
}

Future<Map> getWeather(String apiKey, String city) async {
  String apiUrl =
      "http://api.weatherstack.com/current?access_key=${apiKey}&query=${city}";

  //
  http.Response response = await http.get(Uri.parse(apiUrl));
  return json.decode(response.body);
}

// // Update the widget
// Widget updateTempWidget(String city, apiKey) {
//   return  FutureBuilder(future: getWeather(apiKey, city),builder: (ctx, snapshot) {
//     if (snapshot.hasData) {
//       Object? content = snapshot.data;
//       return Container(
//         child: Column(
//           children: <Widget> [
//             ListTile(
//               title: Text(content["request"])
//             )
//           ],
//         ),
//       );
//     };
//   });
// }

TextStyle cityStyle() {
  return const TextStyle(
      color: Colors.white, fontSize: 15.9, fontStyle: FontStyle.normal);
}

// Adding a style for the temperature
TextStyle tempStyle() {
  return const TextStyle(
      color: Colors.white, fontSize: 40.0, fontStyle: FontStyle.normal);
}
