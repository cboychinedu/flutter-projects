// Importing the necessary modules
import "package:flutter/material.dart";
import 'package:flutter_navigation/pages/main_page.dart';
import 'package:flutter_navigation/routes/routes.dart';

// Running the flutter application
void main() {
  runApp(MyApp());
}

//
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Return the material application
    return const MaterialApp(
        // Adding the route configurations
        initialRoute: RouteManager.homePage,
        onGenerateRoute: RouteManager.generateRoute,
    );
  }

}