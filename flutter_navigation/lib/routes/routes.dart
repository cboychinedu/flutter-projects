// Importing the necessary modules
import "package:flutter/material.dart";
import 'package:flutter_navigation/pages/third_page.dart';

import '../pages/main_page.dart';
import '../pages/second_page.dart';

// Creating the route class
class RouteManager {
  static const String homePage = "/";
  static const String secondPage = "/secondPage";
  static const String thirdPage = "/thirdPage";

  // Setting the route configurations
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case homePage:
        return MaterialPageRoute(builder: (context) => MainPage(),);

      case secondPage:
        return MaterialPageRoute(builder: (context) => SecondPage(), );

      case thirdPage:
        return MaterialPageRoute(builder: (context) => ThirdPage(), );

      default:
        throw FormatException("Route not found, check route again");
    }
  }
}