// Importing the necessary modues
import "package:flutter/material.dart";

// Second Page class
class SecondPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the stateful widget
    return SecondPageState();
  }

}

// Second page state
class SecondPageState extends State <SecondPage> {
  @override
  Widget build(BuildContext context) {
    // Return the body of the second page widget
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Page"),
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 20.0, left: 30.0),
        child: ElevatedButton(
          child: Text("Second Page Section"),
          onPressed: () => { print('button pressed') },
        ),
      ),
    );
  }

}