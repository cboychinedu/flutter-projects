// Importing the necessary modules
import "package:flutter/material.dart";

// Third page class
class ThirdPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the stateful widget
    return ThirdPageState();
  }

}

// Third page state
class ThirdPageState extends State <ThirdPage> {
  @override
  Widget build(BuildContext context) {
    // Return the body of the third page widget
    return Scaffold(
      appBar: AppBar(
        title: Text("Third Page"),
      ),
      // Adding the body
      body: Container(
        margin: const EdgeInsets.only(top: 20.0, left: 30.0),
        child: ElevatedButton(
          child: Text("Third Page Section"),
          onPressed: () => { print("Button Pressed") },
        )
      )
    );
  }

}