// Importing the necessary modules
import "package:flutter/material.dart";
import 'package:flutter_navigation/routes/routes.dart';

// Main page class
class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the stateful widget
    return MainPageState();
  }

}

// Main page state
class MainPageState extends State <MainPage> {
  @override
  Widget build(BuildContext context) {
    // Return the body of the main page widget
    return Scaffold(
      appBar: AppBar(
          title: Text("Main Page"),
    ),
      body: Container(
        margin: const EdgeInsets.only(top: 70.0, left: 50.0),
        child: Column (
          children: <Widget> [
            ElevatedButton(
              child: Text("Main Page Section"),
              onPressed: () => { print('button pressed') },
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(RouteManager.secondPage);
                },
                child: Text("Go to second Page")
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(RouteManager.thirdPage);
                },
                child: Text("Go to third button")),
          ],
        )
      ),
    );
  }

}