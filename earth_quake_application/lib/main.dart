// Importing the necessary modules
import "package:flutter/material.dart";
import "dart:async";
import "dart:convert";
import "package:http/http.dart" as http;

// Running the main function
void main() async {
  //
  List _data = await getJson();

  print(_data);

  // Running the flutter application
  runApp(MaterialApp(
      home: Scaffold(
    appBar: AppBar(
      title: const Text("JSON Parse"),
      centerTitle: true,
      backgroundColor: Colors.deepOrange,
    ),
  )));
}

//
Future<List> getJson() async {
  String apiUrl = "https://earth-quake-server.herokuapp.com/api/data";

  // Making the get request
  http.Response response = await http.get(Uri.parse(apiUrl));

  // Decode the json string
  return json.decode(response.body);
}

// showing alert dialog
void _showOnTabMessage(BuildContext context, String message, String title) {
  // Show the alert dialog
  showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
            title: Text("${title}"),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.pop(context, "Cancel"),
                child: const Text("Cancel"),
              )
            ],
          ));
}
