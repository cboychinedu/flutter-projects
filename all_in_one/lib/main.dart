// Importing the necessary modules
import 'package:flutter/material.dart';
import 'Routes/routes.dart';



//
// void main() {
//   runApp(MaterialApp(
//     title: "Temperature Converter",
//     home: CelsiusPage(),
//   ));
// }
//
//
//
//
//







// Running the main function
void main() {
  runApp(MyApp());
}

// Running the class myApp
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Return the material application
    return const MaterialApp(
      // Adding the route configurations
      title: "Main Application",
      initialRoute: RouteManager.loginPage,
      onGenerateRoute: RouteManager.generateRoute,
    );
  }

}