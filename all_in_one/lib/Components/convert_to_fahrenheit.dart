// Importing the necessary modules
import "package:flutter/material.dart";

// Creating a class for the stateful widget
class FahrenheitPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the stateful widget
    return FahrenheitPageState();
  }
}

// Fahrenheit page state
class FahrenheitPageState extends State <FahrenheitPage> {
  // Creating controllers for the input Kelvin, and Celsius
  final TextEditingController _inputKelvin = TextEditingController();
  final TextEditingController _inputCelsius = TextEditingController();

  // Creating a result variable
  var _result = "0.00";

  // Creating a function for converting Kelvin to Fahrenheit
  void ConvertKelvinToFahrenheit() {
    // Converting kelvin to fahrenheit
    var inputKelvin = _inputKelvin.text.toString();
    double res_kelvin = double.parse(inputKelvin);
    double fahrenheit = 0.00;
    fahrenheit = ((res_kelvin - 273.5) * 9/5) + 32;

    // Changing the state of the result
    setState(() {
      // Changing the result state
      _result = "${fahrenheit}F";
    });
  }

  // Creating a function for converting Celsius to Fahrenheit
  void ConvertCelsiusToFahrenheit() {
    // Converting celsius to fahrenheit
    var inputCelsius = _inputCelsius.text.toString();
    double res_celsius = double.parse(inputCelsius);
    double fahrenheit = 0.00;
    fahrenheit = (res_celsius * 9/5) + 32;

    // Changing the state of the result
    setState(() {
      // Changing the result state
      _result = "${fahrenheit}F";
    });

  }


  @override
  Widget build(BuildContext context) {
    // Returning the body of the fahrenheit
    return Scaffold(
      appBar: AppBar(
        title: const Text("Convert to Fahrenheit"),
        backgroundColor: const Color(0xff5b5b5b),
      ),
      body: Container(
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("images/satcom.jpg"),
              fit: BoxFit.cover,
          )
        ),
        child: Column(
          children: <Widget> [
            Container(
              color: Color(0xff1c1a1a),
              margin: const EdgeInsets.only(top: 20.0, left: 30.0, right: 30.0),
              padding: const EdgeInsets.only(left: 30.0, right: 30.0),
              width: 250.0,
              height: 150.0,
              child: Center(
                child: Text(
                  "${_result}",
                  style: const TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  )
                )
              )
            ),
            // Adding the container for taking in celsius values
            Container(
              width: 300.0,
              padding: const EdgeInsets.only(right: 30.0, left: 20.0),
              margin: const EdgeInsets.only(top: 10.0),
              child: TextField(
                style: const TextStyle(color: Colors.white),
                controller: _inputCelsius,
                decoration: const InputDecoration(
                  hintText: "Value in Celsius C, e.g 56.9C",
                  hintStyle: TextStyle(color: Colors.white),
                  fillColor: Color(0xff1c1a1a),
                  filled: true,
                  icon: Icon(color: Colors.deepOrange, Icons.heat_pump_rounded)
                ),
              )
            ),
            // Adding the container for taking in Kelvin values
            Container(
              width: 300.0,
              padding: const EdgeInsets.only(right: 30.0, left: 20.0),
              margin: const EdgeInsets.only(top: 10.0),
              child: TextField(
                style: const TextStyle(color: Colors.white),
                controller: _inputKelvin,
                decoration: const InputDecoration(
                  hintText: "Value in Kelvin K, e.g 56.9K",
                  hintStyle: TextStyle(color: Colors.white),
                  fillColor: Color(0xff1c1a1a),
                  filled: true,
                  icon: Icon(color: Colors.deepOrange, Icons.heat_pump_rounded)
                ),
              )
            ),
            // Adding the button for converting Celsius to Fahrenheit
            Container(
              margin: const EdgeInsets.only(top: 20.0, left: 35.0),
              width: 220.0,
              height: 40.0,
              child: RaisedButton(
                onPressed: ConvertCelsiusToFahrenheit,
                elevation: 10,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                color: Colors.deepOrange.shade700,
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
                child: const Text(
                  "Convert Celsius to Fahrenheit",
                  style: TextStyle(color: Colors.white70)
                )
              )
            ),
            // Adding the button for converting Kelvin to Fahrenheit
            Container(
              margin: const EdgeInsets.only(top: 20.0, left: 35.0),
              width: 220.0,
              height: 40.0,
              child: RaisedButton(
                onPressed: ConvertKelvinToFahrenheit,
                elevation: 10,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                color: Colors.deepOrange.shade700,
                padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
                child: const Text(
                  "Convert Kelvin to Fahrenheit",
                  style: TextStyle(color: Colors.white70)
                )
              )
            ),

          ],
        ),
      ),

      // Adding the bottom navigation bar
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: const Color(0xff7e7e7e),
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.account_box), label: "Profile"),
            BottomNavigationBarItem(icon: Icon(Icons.add_a_photo), label: "Camera"),
            BottomNavigationBarItem(icon: Icon(Icons.add_photo_alternate), label: "Upload Picture")
          ]),
    );
  }
}


