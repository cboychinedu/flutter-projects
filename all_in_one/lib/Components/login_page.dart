// Importing the necessary modules
import 'package:all_in_one/Routes/routes.dart';
import 'package:flutter/material.dart';
  
// Creating the login stateful widget
class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the login state
    return LoginPageState();
  }

}

// Creating the loginState widget
class LoginPageState extends State<LoginPage> {
  // Creating controllers for the login forms
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();

  // Creating a function for verifying the user
  void VerifyUser() {
    // Setting the username and password
    String username = _username.text.toString();
    String password = _password.text.toString();

    print(password);

    // Checking
    if (username == "chinedu" && password == "1234") {
      // Execute the block of code below
      Navigator.of(context).pushNamed(RouteManager.homePage);
    }

    // Else
    else {

    }
  }

  @override
  Widget build(BuildContext context) {
    // Building the login widget
    return  WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
        backgroundColor: const Color(0xff5b5b5b),
        // actions: [
        //   IconButton(
        //     icon: Image.asset('images/bmi.jpg', width: 500.0, height: 400.0,),
        //     onPressed: () => print("test"),
        //     alignment: Alignment.center,
        //
        //
        //   ),
        // ],
      ),
      body: Container(
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/satcom2.jpg"),
            fit: BoxFit.cover,
          )
        ),
        child: Column(
          children: <Widget> [
            // Image profile
            Container(
              margin: const EdgeInsets.only(top: 50.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
              ),
              child: Image.asset(
                "images/login.jpg",
                width: 400.0,
                height: 170.0,
              ),
            ),
            // Adding the forms
            Container(
              margin: const EdgeInsets.only(top: 20.0),
              padding: const EdgeInsets.only(top: 6.0, left: 10.0, right: 50.0),
              child: Column(
                children: [
                  Container(
                    child: TextField(
                      style: const TextStyle(
                        color: Colors.white,
                      ),
                      controller: _username,
                      decoration: const InputDecoration(
                        hintText: "Username",
                        hintStyle: TextStyle(color:Colors.white),
                        fillColor: Colors.black,
                        filled: true,
                        icon: Icon(color: Colors.blue, Icons.person)
                      ),
                    )
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    child: TextField(
                      style: const TextStyle(
                        color: Colors.white,

                      ),
                      controller: _password,
                      decoration: const InputDecoration(
                        hintText: "Password",
                          hintStyle: TextStyle(color: Colors.white),
                          fillColor: Colors.black,
                          filled: true,
                        icon: Icon(color: Colors.blue, Icons.lock)
                      ),
                      obscureText: true,
                    )
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0, left: 35.0),
                    width: 200.0,
                    child: RaisedButton(
                      elevation: 20,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                      onPressed: VerifyUser,
                      color: Colors.blueAccent,
                      padding: const EdgeInsets.only(top: 20.0, bottom: 20.0, left: 20.0, right: 20.0),
                      child: const Text(
                          "Submit",
                          style: TextStyle(color: Colors.white70)),
                    )
                  ),
                  
                ],
              )
            )
          ],
        )

      ), 
      
      // Adding the bottom navigation bar 
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: const Color(0xff7e7e7e),
          items: const [
        BottomNavigationBarItem(icon: Icon(Icons.account_box), label: "Profile"),
        BottomNavigationBarItem(icon: Icon(Icons.add_a_photo), label: "Camera"),
        BottomNavigationBarItem(icon: Icon(Icons.add_photo_alternate), label: "Upload Picture")
      ]),

    )
    );
  }

}