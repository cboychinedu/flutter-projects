// Importing the necessary modules
import 'package:all_in_one/Components/bmi_calculator.dart';
import "package:flutter/material.dart";

// Creating the bmi stateful widget
class BmiPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the bim state
    return BmiPageState();
  }
}

// Creating the bmistate widget
class BmiPageState extends State<BmiPage> {
  // Creating the object controllers for the age, height, and weight text fields
  final TextEditingController _inputAgeController = TextEditingController();
  final TextEditingController _inputHeightController = TextEditingController();
  final TextEditingController _inputWeightController = TextEditingController();

  // Result
  String _result = "Your BMI: 0.00";
  String WeightValue = "";
  double res = 0.00;
  double _weight = 0.00;
  double _height = 0.00;

  // Creating a method for handling the submit button
  void HandleSubmit() {
    // Checking if the form field is not empty
    if (_inputAgeController.text.isNotEmpty && _inputHeightController.text.isNotEmpty &&
        _inputWeightController.text.isNotEmpty) {
      // Execute the block of code below if the form field is not empty
      _weight = double.parse(_inputWeightController.text);
      _height = double.parse(_inputHeightController.text);

      // Calculate the body mass index
      res = CalculateBmi(_weight, _height);

      // Checking for the category for Normal, Overweight, and Obesity
      // For Normal
      if (res < 24.9) {
        WeightValue = "Normal";
      }
      // Overweight
      else if (res == 24.0 || res < 29.9) {
        WeightValue = "Overweight";
      }
      // Obesity
      else if (res == 29.9 || res >= 30.0 ) {
        // Obesity
        WeightValue = "Obesity";
      }

      // Setting the state
      setState(() {
          // Setting the result to the calculated body mass index value
          _result = "Your BMI: ${res.toStringAsFixed(3)}";
      });
    }

    // Else
    else {
      // Execute the block of code if one of the values are missing
      // Setting the state
      setState(() {
         // The result
          _result = "Age, height or weight is missing.";
      });
      final snackBar = SnackBar(
        content: const Text('Age, height or weight is missing'),
        action: SnackBarAction(
          label: 'Undo',
          onPressed: () {
            // Some code to undo the change.
          },
        ),
      );

      // Find the ScaffoldMessenger in the widget tree
      // and use it to show a SnackBar.
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }


  @override
  Widget build(BuildContext context) {
    // Building the bmi widget
    return Scaffold(
      // Building the appbar
      appBar: AppBar(
        title: const Text("Bmi Calculator"),
        backgroundColor: const Color(0xff5b5b5b),
      ),
      // Adding the body
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("images/satcom.jpg"),
              fit: BoxFit.cover, )
        ),
        child: ListView(
          padding: const EdgeInsets.all(20.0),
          children: [
            // Adding the image
            Image.asset(
                "images/bmi.jpg",
                 height: 200.0,
                  width: 200.0,
                ),
            // Adding the second container to hold the age text field
            Container(
              margin: const EdgeInsets.only(top: 5.0),
              padding: const EdgeInsets.only(right: 70.0, left: 30.0),
              width: 100.0,
              alignment: Alignment.center,
              child: TextField(
                style: const TextStyle(color: Colors.white),
                controller: _inputAgeController,
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  labelText: "Your age",
                  labelStyle: TextStyle(color: Colors.white),
                  fillColor: Color(0xff1c1a1a),
                  filled: true,
                  icon: Icon(color: Colors.deepOrange, Icons.person),
                ),
              )
            ),

            // Adding the third container to hold the height text field
            Container(
                margin: const EdgeInsets.only(top: 6.0),
                padding: const EdgeInsets.only(right: 70.0, left: 30.0),
                width: 100.0,
                alignment: Alignment.center,
                child: TextField(
                  style: const TextStyle(color: Colors.white),
                  controller: _inputHeightController,
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                    labelText: "Your Height",
                    labelStyle: TextStyle(color: Colors.white),
                    fillColor: Color(0xff1c1a1a),
                    filled: true,
                    icon: Icon(color: Colors.deepOrange, Icons.monitor_weight)
                  ),
                )

            ),

            // Adding the fourth container to hold the weight in pounds
            Container(
                margin: const EdgeInsets.only(top: 6.0),
                padding: const EdgeInsets.only(right: 70.0, left: 30.0 ),
                width: 50.0,
                alignment: Alignment.center,
                child: TextField(
                    style: const TextStyle(color: Colors.white),
                    controller: _inputWeightController,
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                        labelText: "Your Weight",
                        labelStyle: TextStyle(color: Colors.white),
                        fillColor: Color(0xff1c1a1a),
                        filled: true,
                        icon: Icon(color: Colors.deepOrange, Icons.monitor_weight),
                    ),
                )
            ),

            // Adding the button container
            Container(
                margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                padding: const EdgeInsets.only(right: 70.0, left: 70.0),
                width: 300.0,
                height: 40.0,
                child: RaisedButton(
                    onPressed: HandleSubmit,
                    elevation: 10,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                    color: Colors.deepOrange,
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
                    child: const Text("Get Bmi Value", style: TextStyle(color: Colors.white)),
                )
            ),

            // Adding the result container
            Container(
              color: Color(0xff1c1a1a),
                margin: const EdgeInsets.only(right: 40.0, left: 40.0),
              padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
              child: Row(
                children: [
                  Text(
                      "${_result}",
                      style: const TextStyle(
                          color: Colors.blue,
                          fontSize: 17.5,
                          fontWeight: FontWeight.w500
                      )
                  ),
                  Text(
                      "  ${WeightValue}",
                      style: const TextStyle(
                          color: Colors.deepOrange,
                          fontSize: 17.5,
                          fontWeight: FontWeight.w500,
                      )
                  )
                ],
              )
            ),

            //
          ],
        )
      ),

      // Adding the bottom navigation bar
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: const Color(0xff7e7e7e),
          items: const [
            BottomNavigationBarItem(icon: Icon(Icons.account_box), label: "Profile"),
            BottomNavigationBarItem(icon: Icon(Icons.add_a_photo), label: "Camera"),
            BottomNavigationBarItem(icon: Icon(Icons.add_photo_alternate), label: "Upload Picture")
          ]),
    );
  }

}