// Importing the necessary modules
import 'package:all_in_one/Routes/routes.dart';
import "package:flutter/material.dart";


// The main page class
class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the stateful home page widget
    return HomePageState();
  }

}

// Home page state
class HomePageState extends State <HomePage> {
  @override
  Widget build(BuildContext context) {
    // Returning the body of the main page widget
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: const Text("Home Page"),
        backgroundColor: const Color(0xff5b5b5b),
      ),
      body: Container(
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("images/satcom.jpg"),
              fit: BoxFit.cover,
          )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 100.0,
                  width: 100.0,
                  margin: EdgeInsets.only(right: 10.0),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(RouteManager.kelvinPage);
                    },
                    elevation: 50.0,
                    color: Colors.green,
                    child: const Text(
                        "Convert \nto Kelvin",
                        style: TextStyle(color: Colors.white70),),
                  ),
                ),
                Container(
                  height: 100.0,
                  width: 100.0,

                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(RouteManager.celsiusPage);
                    },
                    elevation: 50.0,
                    color: Colors.green,
                    child: const Text(
                        "Convert \nto Celsius",
                        style: TextStyle(color: Colors.white70),),
                  )
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 10.0, right: 10.0, left: 10.0),
                  height: 100.0,
                  width: 100.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.deepOrange,
                  ),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(RouteManager.fahrenheitPage);
                    },
                    elevation: 50.0,
                    color: Colors.deepOrangeAccent.shade400,
                    child: const Text(
                        "Convert \nto Fahrenheit",
                        style: TextStyle(color: Colors.white70), ),
                  )
                ),
                Container(
                  margin: EdgeInsets.only(top: 10.0, right: 10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.deepOrange,
                  ),
                  height: 100.0,
                  width: 100.0,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(RouteManager.bmiPage);
                      },
                    elevation: 50.0,
                    color: Colors.deepOrangeAccent.shade400,
                    child: const Text(
                        "Calculate \nBMI",
                          style: TextStyle(color: Colors.white70), ),
                  )
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(right: 110.0, top: 10.0),
              width: 100.0,
              height: 40.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10)
              ),
              child: RaisedButton(
                color: Color(0xffe03e0a),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                    "Logout", style: TextStyle(
                  color: Colors.white,
                  fontSize: 12.0
                ),),
              ),
              )
          ],
        ),
      ),
      // Adding the bottom navigation bar
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: const Color(0xff7e7e7e),
          items: const [
        BottomNavigationBarItem(icon: Icon(Icons.account_box), label: "Profile"),
        BottomNavigationBarItem(icon: Icon(Icons.add_a_photo), label: "Camera"),
        BottomNavigationBarItem(icon: Icon(Icons.add_photo_alternate), label: "Upload Picture")
      ]),
    );
  }

}