// Importing the necessary modules
import "package:flutter/material.dart";

// Creating a function for calculating the body mass index
double CalculateBmi(double weight, double height) {
  // Declaring the result, and some other necessary variables
  double result = 0.00;

  // Converting the height into inches
  height = height * 12;

  // Calculating the body mass index (BMI), and then return the result
  result = weight / (height * height);
  result = result * 703;
  return result;
}