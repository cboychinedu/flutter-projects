// Importing the necessary modules
import "package:flutter/material.dart";

// Creating a class for the stateful widget
class CelsiusPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the stateful widget class
    return CelsiusPageState();
  }

}

// Celsius page state
class CelsiusPageState extends State <CelsiusPage> {
  // Creating controllers for the input kelvin and fahrenheit
  final TextEditingController _inputKelvin = TextEditingController();
  final TextEditingController _inputFahrenheit = TextEditingController();

  // Create a result variable
  var _result = "0.00";

  // Creating a function for converting from kelvin to celsius
  void ConvertKelvinToCelsius() {
    var res = _inputKelvin.text.toString();
    double res_double = double.parse(res);
    res_double = res_double - 273;

    // Changing the state of the result
    setState(() {
      // Changing the result state
      _result = "${res_double}C";
    });
  }
  @override
  Widget build(BuildContext context) {
    // Return the body of the celsius page widget
    return Scaffold(
      appBar: AppBar(
        title: const Text("Convert to Celsius"),
        backgroundColor: const Color(0xff5b5b5b),
      ),
      body: Container(
        alignment: Alignment.center,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/satcom.jpg"),
            fit: BoxFit.cover,
          )
        ),
        child: Column(
          children: [
            Container(
                color: Color(0xff1c1a1a),
                margin: const EdgeInsets.only(top: 20.0, left: 30.0, right: 30.0),
                padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                width: 250.0,
                height: 150.0,
                child: Center(
                    child: Text(
                        "${_result}",
                        style: const TextStyle(
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        )
                    )
                )
            ),
            Container(
                width: 300.0,
                padding: const EdgeInsets.only(right: 30.0, left: 20.0),
                margin: const EdgeInsets.only(top: 10.0),
                child: TextField(
                  style: const TextStyle(color: Colors.white),
                  controller: _inputKelvin,
                  decoration: const InputDecoration(
                      hintText: "Value in Kelvin K, e.g 56.9K",
                      hintStyle: TextStyle(color: Colors.white),
                      fillColor: Color(0xff1c1a1a),
                      filled: true,
                      // labelText: "e.g 56.9K",
                      icon: Icon(color: Colors.deepOrange, Icons.heat_pump_rounded)
                  ),
                )
            ),
            Container(
                width: 300.0,
                padding: const EdgeInsets.only(right: 30.0, left: 20.0),
                margin: const EdgeInsets.only(top: 10.0),
                child: TextField(
                  style: const TextStyle(color: Colors.white),
                  controller: _inputFahrenheit,
                  decoration: const InputDecoration(
                    hintText: "Value in Fahrenheit F, e.g 56.9F",
                    hintStyle: TextStyle(color: Colors.white),
                    fillColor: Color(0xff1c1a1a),
                    filled: true,
                    icon: Icon(color: Colors.deepOrange, Icons.heat_pump_rounded),
                    // labelText: "e.g 56F",
                  ),
                )
            ),
            Container(
                margin: const EdgeInsets.only(top: 20.0, left: 35.0),
                width: 220.0,
                height: 40.0,
                child: RaisedButton(
                    onPressed: ConvertKelvinToCelsius,
                    elevation: 10,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                    color: Colors.deepOrange.shade700,
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
                    child: const Text(
                      "Convert Kelvin to Celsius",
                      style: TextStyle(color: Colors.white70),)
                )
            ),
            Container(
                margin: const EdgeInsets.only(top: 8.0, left: 35.0),
                width: 220.0,
                height: 40.0,
                child: RaisedButton(
                    onPressed: () => print("convert to celsius"),
                    elevation: 10,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                    color: Colors.deepOrange.shade700,
                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 10.0),
                    child: const Text(
                      "Convert Fahrenheit to Celsius",
                      style: TextStyle(color: Colors.white70),)
                )
            ),

          ],
        ),
      ),

      // Adding the bottom navigation bar
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: const Color(0xff7e7e7e),
          items: const [
        BottomNavigationBarItem(icon: Icon(Icons.account_box), label: "Profile"),
        BottomNavigationBarItem(icon: Icon(Icons.add_a_photo), label: "Camera"),
        BottomNavigationBarItem(icon: Icon(Icons.add_photo_alternate), label: "Upload Picture")
      ]),

    );
  }

}