// Importing the necessary modules
import 'package:all_in_one/Components/bmi_page.dart';
import 'package:all_in_one/Components/convert_to_celsius.dart';
import 'package:all_in_one/Components/convert_to_kelvin.dart';
import "package:flutter/material.dart";
import '../Components/convert_to_fahrenheit.dart';
import "../Components/home_page.dart";
import '../Components/login_page.dart';

// Creating the route class
class RouteManager {
  static const String loginPage = "/";
  static const String homePage = "/homePage";
  static const String kelvinPage = "/kelvinPage";
  static const String celsiusPage = "celsiusPage";
  static const String fahrenheitPage = "fahrenheitPage";
  static const String bmiPage = "bmiPage";

  // Setting the route configurations
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case loginPage:
        return MaterialPageRoute(builder: (context) => LoginPage(), );
        break;
      case homePage:
        return MaterialPageRoute(builder: (context) => HomePage(),);
        break;
      case celsiusPage: 
        return MaterialPageRoute(builder: (context) => CelsiusPage(),);
        break;
      case kelvinPage: 
        return MaterialPageRoute(builder: (context) => KelvinPage(),);
        break;
      case bmiPage:
        return MaterialPageRoute(builder: (context) => BmiPage(),);
        break;
      case fahrenheitPage:
        return MaterialPageRoute(builder: (context) => FahrenheitPage(), );
        break;

      default:
        throw FormatException("Route not found, check route again");
    }
  }

}