// Importing the necessary modules
import 'package:flutter/material.dart';

// Extending the stateless widget
class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: const Text("Gestures"),
      ),
      body: const Center(
        child: CustomButton(),
      ),
    );
  }


}

class CustomButton extends StatelessWidget {
  const CustomButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
        onTap: () {
          const snackBar = SnackBar(content: Text("Button Clicked"),
            backgroundColor: Colors.cyan,
            duration: Duration(seconds: 30),);
          // Showing the snackbar when the button is clicked
          Scaffold.of(context).showSnackBar(snackBar);
        },

        child: Container(
            padding: const EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              // color: Theme.of(context).buttonColor,
                color: Colors.deepOrange,
                borderRadius: BorderRadius.circular(5.5)
            ),
            child: const Text("First Button!")
        )
    );
  }

}