// Importing the necessary modules
import 'package:flutter/material.dart';

void buttonPressed() {
  print("Button Pressed");
}

//
class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Container(
        color: Colors.blueAccent,
        alignment: Alignment.center,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: const <Widget> [
            RaisedButton(
                onPressed: buttonPressed,
                child: Text("Press Me", style: TextStyle(color: Colors.green, fontSize: 20),)
            ),
          ],
        )



      // child: const Text("Hello Container", textDirection: TextDirection.ltr,
      //         style: TextStyle(color: Colors.white, fontWeight: FontWeight.w900, fontSize: 19.2 ),),
    );
  }

}