import 'package:flutter/material.dart';
import "./home.dart";

// Running the main function
void main() {
  runApp(const MaterialApp(
    title: "Layouts",
    home: Home(),
  ));
}