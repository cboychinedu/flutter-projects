// Importing the necessary modules
import 'package:flutter/material.dart';

// Creating a class called home
class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Working with scaffold
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green.shade500,
          title: const Text("Wonderful Day"),
          actions: <Widget> [
            IconButton(onPressed: () => debugPrint("icon pressed")  ,icon: Icon(Icons.send)),
            IconButton(onPressed: () => debugPrint("Search Button Pressed"),  icon: Icon(Icons.search)),
            IconButton(onPressed: () => debugPrint("Add call"),icon: Icon(Icons.add_call)),
            RaisedButton(onPressed: () => print("button clicked"),
                child: const Text("Send Message", textDirection:  TextDirection.rtl, style: TextStyle(color: Colors.black,),))
          ],
        ),

        // Other properties
        backgroundColor: Colors.white70,
        body: Container (
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget> [
              const Text("Bonni", style: TextStyle(
                  fontSize: 14.5,
                  fontWeight: FontWeight.w400,
                  color: Colors.deepOrange
              ), ),
              InkWell(
                child: const Text("Button Press"),
                onTap: ()=> debugPrint("Button Pressed"),
              )
            ],
          ),

        )
    );

  }

}