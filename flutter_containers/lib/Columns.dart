// Importing the necessary modules
import 'package:flutter/material.dart';

//
class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Container(
        color: Colors.blueAccent,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            const Text("First Item ", textDirection: TextDirection.ltr, style: TextStyle(color: Colors.white),),
            const Text("Second Item ", textDirection: TextDirection.ltr, style: TextStyle(color: Colors.blue),),

            Container(
                color: Colors.deepOrange,
                alignment: Alignment.bottomCenter,
                child: Column(
                  children: const [
                    RaisedButton(
                      onPressed: null,
                      child: Text("Send"),)
                  ],
                )
            )
          ],
        )




      // child: const Text("Hello Container", textDirection: TextDirection.ltr,
      //         style: TextStyle(color: Colors.white, fontWeight: FontWeight.w900, fontSize: 19.2 ),),
    );
  }

}