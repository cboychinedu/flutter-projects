// Importing the necessary modules
import 'package:flutter/material.dart';

// Creating a class called home
class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Container(
        color: Colors.blueAccent,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget> [
            Text("Item1", textDirection: TextDirection.ltr, style: TextStyle(fontSize: 15.6),),
            Text("Item2", textDirection: TextDirection.ltr, style: TextStyle(fontSize: 15.6),),
            Text("Item3", textDirection: TextDirection.ltr, style: TextStyle(fontSize: 15.6),),
            Text("Item4", textDirection: TextDirection.ltr, style: TextStyle(fontSize: 15.6),),
            Text("Item5", textDirection: TextDirection.ltr, style: TextStyle(fontSize: 15.6),),

            //
            Expanded(
                child: Text("Item3"))
          ],
        )



      // child: const Text("Hello Container", textDirection: TextDirection.ltr,
      //         style: TextStyle(color: Colors.white, fontWeight: FontWeight.w900, fontSize: 19.2 ),),
    );
  }

}