// Importing the necessary modules
import "package:flutter/material.dart";

// Running the main function
void main() {
  runApp(
    MaterialApp(
      title: "Screens",
      home: Home(),
    )
  );
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _nameFieldController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar: AppBar(
       title: const Text("First Screen"),
       centerTitle: true,
       backgroundColor: Colors.green.shade700,
     ),
      body: ListView(
        children: [
          ListTile(
            minLeadingWidth: 40.0,
            title: TextField(
              controller: _nameFieldController,
              decoration: InputDecoration(
                labelText: "Enter your name"
              ),
            ),
          ),
          ListTile(
            title: RaisedButton(
              child: const Text("Send To Next Screen"),
              onPressed: () {
                var router = MaterialPageRoute(builder: (BuildContext context) {
                  return NextScreen(name: _nameFieldController.text,);
                });

                Navigator.of(context).push(router);
              },
            ),
          )
        ],
      ),
    );
  }
}

// The next screen
class NextScreen extends StatefulWidget {
  final String name;
  const NextScreen({Key? key, required this.name}) : super(key: key);


  @override
  State<NextScreen> createState() => _NextScreenState();
}

class _NextScreenState extends State<NextScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Page"),
        backgroundColor: Colors.green.shade700,
        centerTitle: true,
      ),
      body: Column(
        children: [
          ListTile(
            title: Text("Hello, ${widget.name}"),
          ),
        ],
      )
    );
  }
}

