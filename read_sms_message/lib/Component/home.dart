// Importing the necessary modules
import "dart:async";
import "package:flutter/material.dart";
import 'package:sms_advanced/sms_advanced.dart';

class MySms extends StatefulWidget{
  @override
  State createState() {
    // TODO: implement createState
    return MyInboxState();
  }

}

class MyInboxState extends State{
  SmsQuery query = new SmsQuery();
  List messages=[];
  @override
  initState()  {
    // TODO: implement initState
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("SMS Inbox"),
          backgroundColor: Colors.pink,
        ),
        body: FutureBuilder(
          future: fetchSMS() ,
          builder: (context, snapshot)  {

            return ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  height: 20,
                  color: const Color(0xffdcd5e8),
                  indent: 50.0,
                  endIndent: 50.0,
                  thickness: 1.3,
                ),
                itemCount: messages.length,
                itemBuilder: (context,index){
                  return Container(
                    height: 60.0,
                    margin: const EdgeInsets.only(top: 5.0),
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                      onTap: () {
                        showDialog(context: context,
                          builder: (BuildContext context) =>
                              AlertDialog(
                                backgroundColor: Color(0xffcfced6),
                                title: Text("${messages[index].body}"),
                              ),);
                      },
                      leading: Icon(Icons.markunread,color: Colors.pink,),
                      title: Text(messages[index].address),
                      subtitle: Text(messages[index].body,maxLines:2,style: TextStyle(),),
                    ),
                  );
                });
          },)
    );
  }

  fetchSMS()
  async {
    messages = await query.getAllSms;
  }
}

//
showMessage(context, num) {
  // Show the alert dialog
  showDialog(
      // context: context,
      builder: (BuildContext context) =>
          AlertDialog(
            backgroundColor: Color(0xffcfced6),
            title: Text("Hello"),
          ), context: context
  );
}