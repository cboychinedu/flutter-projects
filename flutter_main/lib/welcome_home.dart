// importing the necessary modules
import "package:flutter/material.dart";


class Welcome extends StatelessWidget {
  const Welcome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return const Material(
        color: Colors.blueAccent,
        child: Center(
          child: Text(
            "Welcome Home",
            textDirection: TextDirection.ltr,
            style: TextStyle(fontWeight: FontWeight.w500, fontStyle: FontStyle.italic, fontSize: 34.5),
          ),
        )
    );
  }

}