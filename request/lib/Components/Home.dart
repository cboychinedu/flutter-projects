// Importing the necessary modules
import "package:flutter/material.dart";
import "dart:async";
import "dart:convert";
import "package:http/http.dart" as http;

// Get json data 
Future<List> getJson() async {
    String apiUrl = "http://192.168.180.79:5001/api/data";  
    http.Response response = await http.get(Uri.parse(apiUrl)); 

    return json.decode(response.body); 
}

// Creating the home statelessw widget
class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // Return the home page
    return HomePageState();
  }
}

// Creating the Homepage state
class HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    // Return the home widget
    return Scaffold(
      appBar: AppBar(
        title: const Text("Earth Quake Application"),
        centerTitle: false,
        backgroundColor: Color(0xff0d051a),
      ),
      body: Container(
        color: Color(0xff302a47)
        child: ListView.builder(
          itemCount: data.length,
          padding: const EdgeInsets.all(5.0), 
          itemBuilder: (BuildContext context, int position) {
            if (position.isOdd) {
              return const Divider(
                height: 20,
                color: const Color(0xffdcd5e8), 
                indent: 50.0, 
                endIndent: 50.0, 
                thickness: 1.3,
              ); 
            }
          },
        )
      ),
    );
  }
}
