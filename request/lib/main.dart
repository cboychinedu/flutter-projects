// Importing the necessary modules
import "package:flutter/material.dart";
import "dart:async";
import "dart:convert";
import "package:http/http.dart" as dart;

import 'Components/Home.dart';

// Running the main function
void main() async {
  runApp(MaterialApp(
    title: "Request",
    home: HomePage(),
  ));
}
