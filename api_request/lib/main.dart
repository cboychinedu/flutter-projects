// Importing the necessary modules
import "package:flutter/material.dart";
import "dart:async";
import "dart:convert";
import "package:http/http.dart" as http;

// Running the main function
void main() async {
  //
  List _data = await getJson();

  // print(_data);

  // Running the flutter application
  runApp(MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: const Text("Earth Quake Application"),
            centerTitle: true,
            backgroundColor: const Color(0xff0d051a),
          ),
          body: Container(
              color: const Color(0xff302a47),
              child: ListView.builder(
                itemCount: _data.length,
                padding: const EdgeInsets.all(5.0),
                itemBuilder: (BuildContext context, int position) {
                  if (position.isOdd) {
                    return const Divider(
                      height: 20,
                      color: Color(0xffdcd5e8),
                      indent: 50.0,
                      endIndent: 50.0,
                      thickness: 1.3,
                    );
                  }

                  final int index = position ~/
                      2; // Diving position by 2 and returning an integer result
                  return ListTile(
                      onTap: () => _showOnTabMessage(
                          context,
                          _data[index]["Date"],
                          _data[index]["Time"],
                          _data,
                          index),
                      title: Container(
                        decoration: BoxDecoration(
                            color: const Color(0xff090614),
                            borderRadius: BorderRadius.circular(15)),
                        height: 150.0,
                        width: 300.0,
                        padding:
                            const EdgeInsets.only(left: 20.0, bottom: 20.0),
                        child: Column(children: <Widget>[
                          Container(
                              alignment: Alignment.topLeft,
                              margin:
                                  const EdgeInsets.only(top: 20.0, bottom: 0.0),
                              child: Row(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.only(right: 10.0),
                                    child: CircleAvatar(
                                      backgroundColor: const Color(0xff191226),
                                      child: Text(_data[index]["Magnitude"]),
                                    ),
                                  ),
                                  Text("Type: ${_data[index]["Type"]}",
                                      style: const TextStyle(
                                          color: Color(0xffd3cfe3),
                                          fontSize: 19.5,
                                          fontWeight: FontWeight.bold)),
                                ],
                              )),
                          Container(
                              alignment: Alignment.topLeft,
                              margin:
                                  const EdgeInsets.only(top: 1.0, left: 50.0),
                              child: Row(
                                children: <Widget>[
                                  Text("Depth: ${_data[index]["Depth"]}",
                                      style: const TextStyle(
                                          color: Color(0xffd3cfe3),
                                          fontSize: 13.5)),
                                  Container(
                                    margin: const EdgeInsets.only(left: 5.0),
                                    child: Text(
                                        "Magnitude: ${_data[index]["Magnitude"]} ${_data[index]["Magnitude Type"]}",
                                        style: const TextStyle(
                                          color: Color(0xffe31021),
                                          fontSize: 13.0,
                                        )),
                                  )
                                ],
                              )),
                          Container(
                            margin: const EdgeInsets.only(top: 1.0, left: 50.0),
                            child: Row(
                              children: <Widget>[
                                Container(
                                    child: Text(
                                  "Latitude: ${_data[index]["Latitude"]}",
                                  style: const TextStyle(
                                    fontSize: 13.0,
                                    color: Color(0xffd3cfe3),
                                  ),
                                )),
                                Container(
                                    margin: const EdgeInsets.only(left: 10),
                                    child: Text(
                                        "Longitude: ${_data[index]["Longitude"]}",
                                        style: const TextStyle(
                                          fontSize: 13.0,
                                          color: Color(0xffd3cfe3),
                                        ))),
                              ],
                            ),
                          ),
                          Container(
                              margin:
                                  const EdgeInsets.only(top: 1.0, left: 50.0),
                              alignment: Alignment.centerLeft,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                      alignment: Alignment.centerLeft,
                                      child:
                                          Text("Time: ${_data[index]["Date"]}",
                                              style: const TextStyle(
                                                fontSize: 13.0,
                                                color: Color(0xffd3cfe3),
                                              ))),
                                  Container(
                                      alignment: Alignment.centerLeft,
                                      child:
                                          Text("Date: ${_data[index]["Time"]}",
                                              style: const TextStyle(
                                                fontSize: 13.0,
                                                color: Color(0xffd3cfe3),
                                              ))),
                                ],
                              )),
                        ]),
                      ));
                },
              )))));
}

//
Future<List> getJson() async {
  // String apiUrl = "https://jsonplaceholder.typicode.com/posts";
  String apiUrl = "https://earth-quake-server.herokuapp.com/api/data";
  // String apiUrl = "https://192.168.180.79:5001/api/data";
  // String apiUrl = "http://localhost:5000/api/data";

  // Making the get request
  http.Response response = await http.get(Uri.parse(apiUrl));

  // Decode the json string
  return json.decode(response.body);
}

// showing alert dialog
void _showOnTabMessage(
    BuildContext context, String message, String title, List _data, int index) {
  // Show the alert dialog
  showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
            backgroundColor: Color(0xffcfced6),
            title: Text("${_data[index]["Type"]}"),
            content: Container(
              color: Color(0xffcfced6),
              height: 65.0,
              alignment: Alignment.centerLeft,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                        "Magnitude: ${_data[index]["Magnitude"]}${_data[index]["Magnitude Type"]}"),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text("Depth: ${_data[index]["Depth"]}Meters"),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text("Longitude: ${_data[index]["Longitude"]}"),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text("Latitude: ${_data[index]["Latitude"]}"),
                  )
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                color: Color(0xff0d051a),
                onPressed: () => Navigator.pop(context, "Cancel"),
                child: const Text(
                  "Cancel",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ));
}
